---
title: about
date: 2017-08-31 14:49:18 +0100
img: hc_guitar_butterfly.gif
type: page
menu:
  main:
    weight: 3

---
Hughie has been playing guitars from when he was 9 years old. It's scary to think, but that is over 40 years experience.

He has grade 7 (honours) classical guitar.  

He's played hundreds of gigs in many bands. Favourites were  

* The Inevitable Split, gigging around Leeds and Bradford in the late 80s.  
* King Strut, with the late, lamented Paul Griffiths around the South West, UK, in the mid 90s.
* The Water Pistols - late 90s

Solo playing in pubs, clubs and restaurants is great fun.  
Look over the album [Mantrasphere](http://mantrasphere.co.uk)  

