---
title: home
date: 2017-08-30 17:04:56 +0100
img: hughie_kemble1.jpg
type: page
menu:
  main:
    weight: 1

---
Acoustic, classical, folk, rock, blues.

Free 30 minute taster sessions available.

Over 40 years experience.

Prices:

* 30 mins - £15

* 45 mins - £20

* 60 mins - £25

We can learn together the songs of your choice.

We can get to grips with music theory, composition, performance and singing.

Lessons at his home in Overton.  
(RG25 3DU)

He can travel to your home by arrangement.
